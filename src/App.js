import React, {Component} from 'react';
import './App.scss';
import * as environment from './config/config';

import {ValueService} from './services/value.service';

class App extends Component {

  renderingLabel = 'Rendering [ms]';
  reactForNRecordsLabel = 'React for [n] records';
  reRenderingLabel = 'Re-rendering [ms]';
  placeholder = '---';

  forceRefresh = false;

  numberOfRecordsSelectedByDefault = 10;
  numberOfRecordsOptions = [10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000];

  initialState = {
    valuesData: null,
    highlightKeyword: false,
    getValuesStartTime: null,
    getValuesEndTime: null,
    findValuesStartTime: null,
    findValuesEndTime: null,
    gettingData: false,
    findingData: false,
    numberOfRecords: this.numberOfRecordsSelectedByDefault
  };

  runExperiment = true;
  maxIteration = 10;
  separator = ';';
  experimentData;

  columnCount() {
    return environment.resultTableColumns;
  }

  rowCount() {
    return this.state.valuesData ? Math.ceil(this.state.valuesData.size / this.columnCount()) : 0;
  }

  columnIds() {
    const arr = [];
    for (let i = 0; i < this.columnCount(); i++) {
      arr[i] = i;
    }
    return arr;
  }

  rowIds() {
    const arr = [];
    for (let i = 0; i < this.rowCount(); i++) {
      arr[i] = i;
    }
    return arr;
  }

  constructor(props) {
    super(props);
    this.state = this.initialState;

    this.getValues = this.getValues.bind(this);
    this.findValues = this.findValues.bind(this);
    this.copyToClipboard = this.copyToClipboard.bind(this);
    this.onNumberOfRecordsChange = this.onNumberOfRecordsChange.bind(this);
    this.onNumberOfRecordsSet = this.onNumberOfRecordsSet.bind(this);
    this.setNumberOfRecords = this.setNumberOfRecords.bind(this);
    this.initExperiment = this.initExperiment.bind(this);
    this.reset = this.reset.bind(this);
  }

  reset() {
    this.setState(prevState => ({
      valuesData: null,
      highlightKeyword: false,
      getValuesStartTime: null,
      getValuesEndTime: null,
      findValuesStartTime: null,
      findValuesEndTime: null,
      gettingData: false,
      findingData: false,
      numberOfRecords: prevState.numberOfRecords
    }));
  }

  getValues() {
    this.reset();
    ValueService.getValues(this.state.numberOfRecords)
      .then(
        response => {
          this.setState(prevState => ({
            valuesData: response.data,
            gettingData: true,
            getValuesStartTime: performance.now()
          }));
        }
      );
  }

  findValues() {
    this.setState(prevState => ({
      highlightKeyword: true,
      findingData: true,
      findValuesStartTime: performance.now()
    }));
  }

  getValuesTime() {
    if (this.state.getValuesEndTime && this.state.getValuesStartTime) {
      let result = this.state.getValuesEndTime - this.state.getValuesStartTime;
      return result.toFixed(5);
    } else {
      return this.placeholder;
    }
  }

  findValuesTime() {
    if (this.state.findValuesEndTime && this.state.findValuesStartTime) {
      let result = this.state.findValuesEndTime - this.state.findValuesStartTime;
      return result.toFixed(5);
    } else {
      return this.placeholder;
    }
  }

  onNumberOfRecordsChange(changeEvent) {
    const val = parseInt(changeEvent.target.value);
    this.setNumberOfRecords(val);
  }

  onNumberOfRecordsSet(event) {
    const val = parseInt(event.target.value);
    this.setNumberOfRecords(val);
  }

  setNumberOfRecords(val) {
    this.setState(prevState => ({numberOfRecords: val}));
    this.reset();
  }

  getValueAt(row, col) {
    const index = row * this.columnCount() + col;
    return this.state.valuesData.data[index];
  }

  renderSingleOption(option) {
    return (
      <span key={option}>
        <input type="radio" name="numberOfRecords" value={option} checked={option === this.state.numberOfRecords}
               onChange={this.onNumberOfRecordsChange}/>
        <label>{option}</label>
      </span>
    )
  }

  renderOptions() {
    let arr = [];
    this.numberOfRecordsOptions.forEach((option) => {
      arr.push(this.renderSingleOption(option));
    });
    return arr;
  }

  renderColumns(rowId) {
    let arr = [];
    this.columnIds().forEach((columnId) => {
      arr.push(
        <td key={columnId}
            className={this.state.highlightKeyword && this.getValueAt(rowId, columnId) === this.state.valuesData.keyWord ? 'highlight' : ''}>
          {this.getValueAt(rowId, columnId)}
        </td>
      );
    });
    return arr;
  }

  renderRows() {
    let arr = [];
    this.rowIds().forEach((rowId) => {
      arr.push(
        <tr key={rowId}>
          {this.renderColumns(rowId)}
        </tr>
      );
    });
    return arr;
  }

  copyToClipboard() {
    const data =
      // `${this.reactForNRecordsLabel}\t${this.renderingLabel}\t${this.reRenderingLabel}
      `${this.state.valuesData.size}\t${this.getValuesTime()}\t${this.findValuesTime()}`;

    this.copyStringToClipboard(data);
  }

  copyStringToClipboard(str) {
    // Create new element
    const el = document.createElement('textarea');
    // Set value (string to be copied)
    el.value = str;
    // Set non-editable to avoid focus and move outside of view
    el.setAttribute('class', 'clipboard-data');
    el.setAttribute('readonly', '');
    document.body.appendChild(el);
    // Select text inside element
    el.select();
    // Copy text to clipboard
    document.execCommand('copy');
    // Remove temporary element
    document.body.removeChild(el);
  }

  componentDidMount() {
    setTimeout(() => {
      this.initExperiment();
    }, 500);
  }

  componentDidUpdate() {
    if (this.state.gettingData && this.state.valuesData) {
      this.state.getValuesEndTime = performance.now();
      this.state.gettingData = false;
      this.forceRefresh = true;
      this.continueExperiment();
    } else if (this.state.findingData) {
      this.state.findingData = false;
      this.state.findValuesEndTime = performance.now();
      this.forceRefresh = true;
      this.completeExperiment();
    }

    if (this.forceRefresh) {
      this.setState(this.state);
      this.forceRefresh = false;
    }
  }

  render() {
    return (
      <div>
        <nav className="nav-query">
          <div>
            Set number of records
            <input type="number"
                   name="numberOfRecords"
                   onChange={this.onNumberOfRecordsSet}/>
            <div className="nav-query-records-selector">
              {this.renderOptions()}
            </div>
          </div>
          <div className="nav-query-button-group">
            <button onClick={this.getValues}>Get</button>
            <button onClick={this.findValues} disabled={!this.state.valuesData}>Find</button>
            {/*<button onClick={this.copyToClipboard}*/}
                    {/*disabled={!this.state.valuesData && !this.state.getValuesStartTime && !this.state.findValuesEndTime}>*/}
              {/*Copy results to clipboard*/}
            {/*</button>*/}
            <button onClick={this.reset} disabled={!this.state.valuesData}>Reset</button>
          </div>
        </nav>
        <div>
          <div className="result-data-info">
            <div className="col-1">
              <h3>Data info</h3>
              <ul>
                <li>Keyword: {this.state.valuesData ? this.state.valuesData.keyword : this.placeholder}</li>
                <li>Keyword found
                  count: {this.state.valuesData ? this.state.valuesData.keyWordCount : this.placeholder}</li>
                <li>Min keyword: {this.state.valuesData ? this.state.valuesData.min : this.placeholder}</li>
                <li>Max keyword: {this.state.valuesData ? this.state.valuesData.max : this.placeholder}</li>
                <li>Total records: {this.state.valuesData ? this.state.valuesData.size : this.placeholder}</li>
              </ul>
            </div>
            <div className="col-2">
              <table className="result-table">
                <thead>
                <tr>
                  <th>{this.reactForNRecordsLabel}</th>
                  <th>{this.renderingLabel}</th>
                  <th>{this.reRenderingLabel}</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td>{this.state.valuesData ? this.state.valuesData.size : this.placeholder}</td>
                  <td>{this.getValuesTime()}</td>
                  <td>{this.findValuesTime()}</td>
                </tr>
                </tbody>
              </table>
            </div>
          </div>

          <table className="result-table">
            <tbody>
            {this.renderRows()}
            </tbody>
          </table>
          {/*columnCount: {this.columnCount()}<br/>*/}
          {/*rowCount: {this.rowCount()}<br/>*/}
          {/*columnIds: {this.columnIds()}<br/>*/}
          {/*rowIds: {this.rowIds()}<br/>*/}
        </div>

      </div>
    );
  }

  // Experiment
  hasExperimentEnded() {
    return (this.experimentData.recordIndex >= this.numberOfRecordsOptions.length);
  }

  initExperiment() {
    if (!this.runExperiment) {
      return;
    }

    let initData = {
      iterationIndex: 0, // experiment run for same records number
      recordIndex: 0, // changes data requested
      data: `frontend${this.separator}numberOfRecords${this.separator}loadingtime${this.separator}rendeting${this.separator}re-rendering`
    };

    this.experimentData = JSON.parse(sessionStorage.getItem('reactExperimentData'));
    if (!this.experimentData) {
      this.experimentData = initData;
      sessionStorage.setItem('reactExperimentData', JSON.stringify(this.experimentData));
    }

    if (this.experimentData.iterationIndex >= this.maxIteration) {
      this.experimentData.recordIndex++;
      this.experimentData.iterationIndex = 0;
    }

    if (!this.hasExperimentEnded()) {
      this.setNumberOfRecords(this.numberOfRecordsOptions[this.experimentData.recordIndex]);

      // EXPERIMENT: Start
      this.experimentData.iterationIndex++;
      this.getValues();
    } else {
      console.log('Experiment has ended');
      console.log('Final experiment results:');
      console.log(this.experimentData.data);
      // this.copyStringToClipboard(this.experimentData.data);
      // console.log('Copied to clipboard');
    }
    // Remove session data
    // sessionStorage.removeItem('reactExperimentData');
  }

  continueExperiment() {
    if (!this.runExperiment) {
      return;
    }

    // EXPERIMENT: Find results
    setTimeout(() => {
      this.findValues();
    }, 1000);
  }

  completeExperiment() {
    if (!this.runExperiment) {
      return;
    }

    // EXPERIMENT: Get all experiment data
    setTimeout(() => {
      this.experimentData.data = this.experimentData.data + this.prepareExperimentResult();
      sessionStorage.setItem('reactExperimentData', JSON.stringify(this.experimentData));
      window.location.reload(true);
    }, 1000);
  }

  prepareExperimentResult() {
    let firstPaintTime = performance.timing.domComplete - performance.timing.navigationStart;
    return `\nreact${this.separator}${this.state.valuesData.size}${this.separator}${firstPaintTime}${this.separator}${this.getValuesTime()}${this.separator}${this.findValuesTime()}`;
  }

  // Experiment End
}

export default App;
